#ifndef __CONTROLCARTESIANLINEARPOSITION_H__
#define __CONTROLCARTESIANLINEARPOSITION_H__

#include "ikcl/ikclDefines.h"
#include <ctrl_toolbox/VirtualFrame.h>
#include <eigen3/Eigen/Dense>
#include <rml/RML.h>
#include <tpik/TPIKlib.h>
namespace ikcl {

/**
 * @brief The ControlCartesianLinearPosition class implementing the linear position task.
 * @details ikcl class aimed at implementing the position control task for the constructor input frame.
 * The class uses the rml::RobotModel in order to compute the needed jacobians and parameters.
 * The class derives from tpik::CartesianTask and allows the position control of the desired frame in a task
 * priority framework.\n
 * \f$ e=\textrm{rml::CartesianError}(wTf,wTg)\f$\n
 * \f$ \dot{x}= \gamma \cdot e_{lin} \f$\n
 * \f$ J=J_{g, lin}\f$\n
 * \f$ A_{i}=\sigma_{\uparrow}(x_{min}, x_{max}) \f$ where \f$ x_{min}, x_{max}\f$ are defined in the
 * tpik::increasingBellShape struct.\n
 * Depending on the task type the task could either be :Equality, Inequality increasing, Inequality Decreasing. The
 * name increasing and decreasing derives from the activation function which will be an increasing bell shape in the
 * first case and a decreasing bell shaped in the second one.
 */

class ControlCartesianDistance : public tpik::CartesianTask {
public:
    /**
     * @brief ControlCartesianLinearPosition class constructor
     * @param taskID id of the task
     * @param robotModel shared ptr to the robot model
     * @param frameID id of the frame to control
     * @param observerID id of the observer
     * @param useVirtualGoal boolean stating whether using the virtual goal
     * @param taskType enum defining wheter the task is equality or inequality
     * @param projectorType defining whether the task is projected on plane, line or default
     */
    ControlCartesianDistance(std::string taskID, std::shared_ptr<rml::RobotModel> robotModel, std::string frameID,
        bool useVirtualGoal, tpik::CartesianTaskType taskType, tpik::ProjectorType projectorType);

    /* @brief ~ControlCartesianLinearPosition default de constructor
     */
    ~ControlCartesianDistance();

    /**
     * @brief Method setting goal matrix wrt to the input frame
     * @param wTg transformation matrix from world to goal
     */

    void SetwTg(Eigen::TransfMatrix T, const std::string frameID);

    /**
     * @brief Method setting whether using the virtual frame
     * @param useVirtualFrame true for using the virtual frame false otherwise
     */
    void SetUseVirtualFrame(bool useVirtualFrame);

    /**
     * @brief Method setting the sample time .
     * @note The sample time must be instantiated when using the virtual frame
     * @param ts sample time
     */
    void SetSampleTime(double ts);

    /**
     * @brief Method returning the distance to the goal, if no virtual frame is active it coincides
     * with the control variable of the task.
     * @return goal distance.
     */
    Eigen::VectorXd GetDistanceToGoal();

    /**
     * @brief Method setting the virtual frame gain.
     * @note The virtual frame gain must be instantiated when using the virtual frame
     * @param virtualFrameGain
     */
    void SetVirtualFrameGain(double virtualFrameGain);

    /**
     * @brief Method setting the distance hence the task control variable.
     * @param distance
     * @param frameID id of the frame w.r.t. the distance is expressed.
     */
    void SetDistance(Eigen::Vector3d distance, std::string frameID);

    /**
     * @brief Method to update the task variables.
     * @details Implememntation of the pure virtual method of the base class tpik::CartesianTask used to update the task
     * variables
     * i.e. the jacobian, activation function and reference. Exception are thrown if the task parameters have not been
     * initialized.
     */
    void Update() throw(tpik::ExceptionWithHow) override;

    /**
     * @brief Method returning the shared ptr to the class virtual frame
     * @return  shared ptr to the class virtual frame
     */
    std::shared_ptr<ctb::VirtualFrame> GetVirtualFrame();

    /**
     * @brief Method setting the projector frame,
     * @param projectorFrame projector frame
     * @param projectorFrameID id of the frame wrt which the projector frame is expressed
     * @note IN CASE OF PLANE, Z ALONG THE NORMAL TO THE PLANE, IN CASE OF LINE X ALONG THE DIRECTION
     */
    void SetProjectorFrame(Eigen::RotMatrix projectorFrame, std::string projectorFrameID);

    /**
     * @brief Overload of the cout operator.
     * */
    friend std::ostream& operator<<(std::ostream& os, ControlCartesianDistance const& controlCartesianLinearPosition)
    {
        os << "\033[1;37m"
           << "CONTROL CARTESIAN LINEAR POSITION " << (tpik::CartesianTask&)controlCartesianLinearPosition;
        os << std::setprecision(4) << "\033[1;37m"
           << "wTg \n"
           << "\033[0m" << controlCartesianLinearPosition.goalFrameTgoal_ << "\n"
           << "\033[1;37m"
           << "frameID \n"
           << "\033[0m" << controlCartesianLinearPosition.frameIDControl_ << "\n"
           << "\033[1;37m"
           << "use virtual goal \n"
           << "\033[0m" << controlCartesianLinearPosition.useVirtualGoal_ << "\n";

        if (controlCartesianLinearPosition.useVirtualGoal_) {
            os << "\033[1;37m"
               << "wTvrtgoal \n"
               << "\033[0m" << controlCartesianLinearPosition.goalFrameTcomputedGoal_ << "\n";
        }
        return os;
    }

protected:
    /**
     * @brief Method updating the task Jacobian.
     * @details Implementation of the pure virtual method of the base class tpik::CartesianTask  that updates the task
     * jacobians.
     */
    void UpdateJacobian() override;

    std::string frameIDControl_; //!< The id of the frame to control
    Eigen::TransfMatrix goalFrameTgoal_; //!< The transformation matrix from world to goal
    std::shared_ptr<ctb::VirtualFrame> virtualFrame_; //!< The shared ptr to the virtual frame object
    std::shared_ptr<rml::RobotModel> robotModel_; //!< The rml::RobotModel
    bool useVirtualGoal_{
        true
    }; //!< The boolean stating whether using the virtual goal, true if the virtual goal is enabled, false otherwise
    Eigen::TransfMatrix goalFrameTcomputedGoal_; //!< The transformation matrix between the world and the virtual goal
    Eigen::Vector3d distanceBodyFrame_; //!< The distance expressed w.r.t. th body frame
    Eigen::Vector3d distance_; //!< The distance set
    std::string frameIDDistance_; //!< The frame ID w.r.t. which the distance has been set
    std::string frameIDGoal_; //!< The frame ID w.r.t. which the goal frame has been set
    Eigen::RotMatrix projectorFrame_; //!< The rotation matrix of the projector frame
    std::string projectorFrameID_; //!< The ID of the frame w.r.t. the projector rotation matrix has been set
    GoalType goalType_; //!< Class enum stating whether the goal has been set either as distance vector or tranformation
                        //!matrix
};
}
#endif
