#include "ikcl/ControlCartesianDistance.h"

namespace ikcl {

ControlCartesianDistance::ControlCartesianDistance(std::string taskID, std::shared_ptr<rml::RobotModel> robotModel,
    std::string frameID, bool useVirtualGoal, tpik::CartesianTaskType taskType, tpik::ProjectorType projectorType)
    : tpik::CartesianTask(taskID, robotModel->GetTotalDOFs(), taskType, projectorType)
    , frameIDControl_(frameID)
    , robotModel_(robotModel)
    , useVirtualGoal_(useVirtualGoal)

{

    if (useVirtualGoal) {
        virtualFrame_ = std::make_shared<ctb::VirtualFrame>(ctb::VirtualFrame(ctb::VirtualFrame::VFType::Linear));
    }
    switch (projectorType_) {
    case (tpik::ProjectorType::OnLine): {
        taskSpace_ = 1;
        break;
    }
    case (tpik::ProjectorType::OnPlane): {
        taskSpace_ = 2;
        break;
    }
    case (tpik::ProjectorType::Default): {
        break;
    }
    }
    Ai_.setZero(taskSpace_, taskSpace_);
    x_dot_.setZero(taskSpace_);
    J_.setZero(taskSpace_, DoF_);
    Eigen::VectorXd reference;
    reference.setZero(taskSpace_);
    SetControlVectorReference(reference);
    goalType_ = GoalType::NotSet;
}

ControlCartesianDistance::~ControlCartesianDistance() {}

void ControlCartesianDistance::SetwTg(Eigen::TransfMatrix T, const std::string frameID)
{
    goalFrameTgoal_ = T;
    frameIDGoal_ = frameID;

    if (useVirtualGoal_) {
        virtualFrame_->ResetState(robotModel_->GetTransformation(frameIDControl_));
    }
    goalType_ = GoalType::TransformationMatrix;
}

void ControlCartesianDistance::SetUseVirtualFrame(bool useVirtualFrame) { useVirtualGoal_ = useVirtualFrame; }

void ControlCartesianDistance::SetSampleTime(double ts)
{
    if (useVirtualGoal_) {
        virtualFrame_->SetSampleTime(ts);
    }
}

void ControlCartesianDistance::SetVirtualFrameGain(double virtualFrameGain)
{
    if (useVirtualGoal_) {
        virtualFrame_->SetGain(virtualFrameGain);
    }
}

Eigen::VectorXd ControlCartesianDistance::GetDistanceToGoal()
{
    Eigen::VectorXd distance;
    if (useVirtualGoal_) {
        Eigen::TransfMatrix T = robotModel_->GetTransformation(frameIDControl_);
        distance = rml::CartesianError(T, goalFrameTgoal_).GetSecondVect3();
    } else {
        distance = GetControlVariable();
    }
    return distance;
}

void ControlCartesianDistance::SetDistance(Eigen::Vector3d distance, std::string frameID)
{
    if (frameID == rml::FrameID::ProjectorFrameID) {
        distance_ = projectorFrame_ * distance;
        frameIDDistance_ = projectorFrameID_;
    } else {
        distance_ = distance;
        frameIDDistance_ = frameID;
    }
    goalType_ = GoalType::Distance;
}

void ControlCartesianDistance::Update() throw(tpik::ExceptionWithHow)
{
    CheckInitialization();
    Eigen::TransfMatrix bodyTDistance;
    switch (goalType_) {

    case GoalType::NotSet: {
        tpik::NotInitialziedTaskParameterException notInitializedDistance;
        std::string how = "[ControlCartesianPosition] Not Initialized either wTg or distance for task " + ID_
            + "\n use SetwTg() or SetDistance()";
        notInitializedDistance.SetHow(how);
        throw(notInitializedDistance);
    }

    case GoalType::TransformationMatrix: {
        Eigen::TransfMatrix gaolFrame_T_controlFrame;
        gaolFrame_T_controlFrame = robotModel_->GetTransformationFrames(frameIDGoal_, frameIDControl_);
        if (useVirtualGoal_) {
            virtualFrame_->Compute(gaolFrame_T_controlFrame, goalFrameTgoal_, goalFrameTcomputedGoal_);
        } else {
            goalFrameTcomputedGoal_ = goalFrameTgoal_;
        }

        distance_ = rml::CartesianError(gaolFrame_T_controlFrame, goalFrameTcomputedGoal_).GetSecondVect3();
        bodyTDistance = robotModel_->GetTransformationFrames(robotModel_->GetBodyFrameID(), frameIDGoal_);
        break;
    }

    case GoalType::Distance: {
        bodyTDistance = robotModel_->GetTransformationFrames(robotModel_->GetBodyFrameID(), frameIDDistance_);
        break;
    }
    }

    distanceBodyFrame_ = bodyTDistance.GetRotMatrix() * distance_;

    if (!(projectorType_ == tpik::ProjectorType::Default)) {
        bodyFrame_T_parameterProjector_
            = robotModel_->GetTransformationFrames(robotModel_->GetBodyFrameID(), frameIDProjector_);
    }
    UpdateProjector();
    distanceBodyFrame_ = P_ * distanceBodyFrame_;
    Eigen::VectorXd controlVectorBodyFrame;
    switch (projectorType_) {
    case (tpik::ProjectorType::Default): {
        controlVectorBodyFrame = distanceBodyFrame_;
        break;
    }
    case (tpik::ProjectorType::OnPlane): {
        distanceBodyFrame_ = projectorFrame_ * distanceBodyFrame_;
        controlVectorBodyFrame = distanceBodyFrame_.head(2);
        break;
    }
    case (tpik::ProjectorType::OnLine): {
        distanceBodyFrame_ = projectorFrame_ * distanceBodyFrame_;
        controlVectorBodyFrame = distanceBodyFrame_.head(1);
        break;
    }
    }

    SetControlVariable(controlVectorBodyFrame);
    UpdateJacobian();
    UseErrorNormJacobian();
    UpdateReference();
    UpdateInternalActivationFunction();
    SaturateReference();
}

std::shared_ptr<ctb::VirtualFrame> ControlCartesianDistance::GetVirtualFrame() { return virtualFrame_; }

// protected:

void ControlCartesianDistance::SetProjectorFrame(Eigen::RotMatrix projectorFrame, std::string projectorFrameID)
{
    projectorFrame_ = projectorFrame;
    projectorFrameID_ = projectorFrameID;
}

void ControlCartesianDistance::UpdateJacobian()
{
    Eigen::MatrixXd JTemp;
    JTemp = -P_ * robotModel_->GetCartesianJacobian(frameIDControl_).block(3, 0, 3, DoF_);
    switch (projectorType_) {

    case (tpik::ProjectorType::Default): {
        J_ = JTemp;
        break;
    }

    case (tpik::ProjectorType::OnPlane): {
        Eigen::RotMatrix bodyTprojectorFrame;
        bodyTprojectorFrame
            = robotModel_->GetTransformationFrames(robotModel_->GetBodyFrameID(), projectorFrameID_).GetRotMatrix()
            * projectorFrame_;

        JTemp = bodyTprojectorFrame.transpose() * JTemp;

        J_ = JTemp.block(0, 0, 2, DoF_);
        break;
    }

    case (tpik::ProjectorType::OnLine): {
        Eigen::RotMatrix bodyTprojectorFrame;
        bodyTprojectorFrame
            = robotModel_->GetTransformationFrames(robotModel_->GetBodyFrameID(), projectorFrameID_).GetRotMatrix()
            * projectorFrame_;
        JTemp = bodyTprojectorFrame.transpose() * JTemp;
        J_ = JTemp.block(0, 0, 1, DoF_);
        break;
    }
    }
}
}
