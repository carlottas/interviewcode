
#include <auv_msgs/NavigationStatus.h>
#include <eigen3/Eigen/Dense>
#include <geometry_msgs/PoseStamped.h>
#include <rml/RML.h>
#include <robust_control/ArmModel/RobustRobotModelFactory.h>
#include <robust_control/Defines/RobustGeometryDefines.h>
#include <robust_control/Defines/RobustRosDefines.h>
#include <robust_control/Defines/RobustSimulationDefines.h>
#include <robust_control/RobustFunctions/RobustConfiguration.h>
#include <robust_control/RobustFunctions/RobustFunctions.h>
#include <robust_control/ToolPosition.h>
#include <robust_msgs/AUVPosition.h>
#include <robust_msgs/DVL.h>
#include <robust_msgs/Event.h>
#include <robust_msgs/NodulePositionLanding.h>
#include <robust_msgs/NoduleWithNormal.h>
#include <robust_msgs/OrientationDepth.h>
#include <robust_msgs/Status.h>
#include <ros/ros.h>
#include <std_msgs/Empty.h>

// Dynamic reconfigure includes.
#include "robust_simulations/second_order_filterConfig.h"
#include <dynamic_reconfigure/server.h>

void DVLCallBack(const robust_msgs::DVLConstPtr& msg);

void CompassCallBack(const robust_msgs::OrientationDepthConstPtr& msg);

void StartDemoLanding(const std_msgs::EmptyConstPtr& msg);

void ConfigCallback(robust_simulations::second_order_filterConfig& config, uint32_t level);

void ToolPositionCallBack(const robust_control::ToolPositionConstPtr& msg);

void StartDemoTool(const std_msgs::EmptyConstPtr& msg);

void KclEventCallBack(const robust_msgs::EventConstPtr& msg);

void NodulePositionLandinCallBack(const robust_msgs::NodulePositionLandingConstPtr& msg);

void NodulePositionManipulationCallBack(const robust_msgs::NoduleWithNormalConstPtr& msg);

void CameraPositionCallBack(const geometry_msgs::PoseStampedConstPtr& msg);

void ConfigureKCL(const std_msgs::EmptyConstPtr& msg);
static Eigen::Vector3d linearVelocity;
static double altitude;
static Eigen::VectorXd depth;
static rml::EulerRPY rpy;
static rml::EulerRPY rpy_old;
static std::shared_ptr<rml::RobotModel> robot_model;
static bool publish_nodule_position_land;
static Eigen::Vector3d position_world;
static double floor_position;
static bool movement_finished;
static SecondOrderFilter depthFilter;
static SecondOrderFilter xyFilter;
static SecondOrderFilter angularDerivativesFilter;
static double lower_folaga_distance;
static bool publish_nodule_manipulation;
static int velocity_input;
static Eigen::TransfMatrix base_T_tool;
static Eigen::Vector3d DVLVelocity;
static bool configure;
int main(int argc, char** argv)
{
    ros::init(argc, argv, "navigation_filter_simulated_node");

    // Dynamic Reconfiguration
    boost::recursive_mutex config_mutex;
    dynamic_reconfigure::Server<robust_simulations::second_order_filterConfig> dr_srv(config_mutex);
    dynamic_reconfigure::Server<robust_simulations::second_order_filterConfig>::CallbackType cb;
    ros::NodeHandle nh;
    double ts;
    int rate = 10;
    ts = 1.0 / rate;
    ros::Rate loop_rate(rate);
    ros::NodeHandle private_node_handle_("~");
    publish_nodule_position_land = false;
    movement_finished = false;
    bool simulate_camera_outputs_landing;
    bool simulate_camera_outputs_manipulation;
    std::vector<double> nodule_position_base_frame_vector;
    Eigen::Vector3d nodule_position_pool_frame;
    std::vector<double> nodule_position_world_frame_vector;
    velocity_input = 0;
    Eigen::VectorXd zdot_filtered;
    altitude = 2.0;
    depth.resize(1);
    depth(0) = 0.1;
    position_world.setZero();
    linearVelocity.setZero();
    rpy.SetPitch(0.0);
    rpy.SetRoll(0.0);
    rpy.SetYaw(0.0);
    rpy_old = rpy;
    configure = false;
    ros::Publisher navigation_filter_pub
        = nh.advertise<auv_msgs::NavigationStatus>(robust::rosTopic::auv_feedback_navigation_filter.c_str(), 10);
    ros::Publisher auv_land_nodule
        = nh.advertise<robust_msgs::NodulePositionLanding>(robust::rosTopic::nodule_position_landing, 1000);
    ros::Publisher auv_land_comand = nh.advertise<std_msgs::Empty>(robust::rosTopic::land_comand, 1000);
    ros::Publisher hold_pub = nh.advertise<std_msgs::Empty>(robust::rosTopic::hold_command, 1000);
    ros::Subscriber dvl_sub = nh.subscribe(robust::rosTopic::dvl_topic, 1, DVLCallBack);
    ros::Subscriber compass_sub = nh.subscribe(robust::rosTopic::compass_topic, 1, CompassCallBack);
    ros::Subscriber arm_tool_position_sub = nh.subscribe(robust::rosTopic::tool_position_log, 1, ToolPositionCallBack);
    ros::Subscriber KCL_event_sub = nh.subscribe(robust::rosTopic::KCL_event_topic, 1, KclEventCallBack);
    ros::Publisher arm_do_step = nh.advertise<std_msgs::Empty>(robust::rosTopic::do_step_to_nodule, 1000);
    ros::Publisher arm_nodule_position
        = nh.advertise<robust_msgs::NoduleWithNormal>(robust::rosTopic::nodule_position_for_manipulation, 100);
    ros::Publisher arm_go_to_pre_inspection
        = nh.advertise<std_msgs::Empty>(robust::rosTopic::KCL_go_to_preinspection, 100);
    ros::Subscriber publish_nodule_position_manipulation_sub
        = nh.subscribe(robust::rosTopic::start_demo_move_tool, 1, StartDemoTool);
    ros::Subscriber start_demo_landing_sub = nh.subscribe(robust::rosTopic::start_demo_landing, 1, StartDemoLanding);
    ros::Subscriber nodule_position_landing_sub
        = nh.subscribe(robust::rosTopic::nodule_position_landing, 1, NodulePositionLandinCallBack);
    ros::Subscriber nodule_position_manipulation_sub
        = nh.subscribe(robust::rosTopic::nodule_position_for_manipulation, 1, NodulePositionManipulationCallBack);
    ros::Subscriber camera_position_sub = nh.subscribe(robust::rosTopic::camera_position, 1, CameraPositionCallBack);
    ros::Subscriber configure_kcl_sub = nh.subscribe(robust::rosTopic::configure_parameters_KCL, 1, ConfigureKCL);
    std::string root_folder_input;
    private_node_handle_.param(robust::rosParameters::root_folder_input, root_folder_input,
        std::string("/home/robust/catkin_ws/src/robust_control"));

    private_node_handle_.param(robust::rosParameters::lower_folaga_distance_COM, lower_folaga_distance, 0.348);
    private_node_handle_.getParam(
        robust::rosParameters::nodule_position_world_frame, nodule_position_world_frame_vector);

    private_node_handle_.param(robust::rosParameters::floor_position, floor_position, 2.2);
    private_node_handle_.getParam(robust::rosParameters::nodule_position_base_frame, nodule_position_base_frame_vector);
    private_node_handle_.param(
        robust::rosParameters::simulate_camera_outputs_landing, simulate_camera_outputs_landing, false);
    private_node_handle_.param(
        robust::rosParameters::simulate_camera_outputs_manipulation, simulate_camera_outputs_manipulation, false);
    nodule_position_pool_frame = FromVectorToEigen(nodule_position_world_frame_vector);
    std::string conf_path_frames = root_folder_input + "/FramesAndConstraints.conf";
    robust::RobustRobotModelFactory robust_robot_model_factory(conf_path_frames);
    robust_robot_model_factory.InitializeRobotModel();
    robot_model = robust_robot_model_factory.GetRobotModel();

    Eigen::TransfMatrix camera_T_base = robot_model->GetTransformationFrames(robust::robotModelID::cameraFrame_auv,
        robust::robotModelID::robot_base_id);
    Eigen::Vector3d position_base_frame = FromVectorToEigen(nodule_position_base_frame_vector);

    Eigen::TransfMatrix colorCamera_T_camera;

    rml::EulerRPY rpy_color_camera(0.0, 0.0, -M_PI / 2);
    colorCamera_T_camera.SetRotMatrix(rpy_color_camera.ToRotMatrix());

    Eigen::TransfMatrix world_T_pool;
    rml::EulerRPY rpy_world_pool(0.0, 0.0, 235 * (M_PI / 180));
    world_T_pool.SetRotMatrix(rpy_world_pool.ToRotMatrix());
    int iteration = 0;
    publish_nodule_manipulation = false;
    cb = boost::bind(ConfigCallback, _1, _2);
    dr_srv.setCallback(cb);

    DVLVelocity.setZero(3);
    Eigen::Vector3d xyz_position;
    xyz_position.setZero();
    while (nh.ok()) {
        private_node_handle_.getParam(
            robust::rosParameters::simulate_camera_outputs_landing, simulate_camera_outputs_landing);
        private_node_handle_.getParam(
            robust::rosParameters::simulate_camera_outputs_manipulation, simulate_camera_outputs_manipulation);
        auv_msgs::NavigationStatus navigation_status_msg;
        Eigen::Vector3d rpy_derivatives = angularDerivativesFilter.Filter(rpy.ToVect3());
        Eigen::Vector3d omega_t = rpy.GetOmega(rpy_derivatives); // Projected on the vehicle itself
        zdot_filtered = depthFilter.Filter(depth);
        Eigen::Vector3d xyz_dot_world = xyFilter.Filter(position_world);
        xyz_position += DVLVelocity * ts;
        Eigen::TransfMatrix w_T_auv;
        w_T_auv.SetTransl(position_world);
        w_T_auv.SetRotMatrix(rpy.ToRotMatrix());
        robot_model->SetBodyFramePosition(w_T_auv);

        switch (velocity_input) {

        case 0: {

            linearVelocity.setZero();
        } break;

        case 1: {

            Eigen::Vector3d xyz_dot_auv = w_T_auv.GetRotMatrix().transpose() * xyz_dot_world;
            linearVelocity = xyz_dot_auv;
        } break;

        case 2: {
            linearVelocity = DVLVelocity;
        } break;
        }
        navigation_status_msg.orientation.x = rpy.GetRoll();
        navigation_status_msg.orientation.y = rpy.GetPitch();
        navigation_status_msg.orientation.z = rpy.GetYaw();
        navigation_status_msg.position.north = position_world(0);
        navigation_status_msg.position.east = position_world(1);
        navigation_status_msg.position.depth = position_world(2);
        navigation_status_msg.orientation_rate.x = omega_t(0);
        navigation_status_msg.orientation_rate.y = omega_t(1);
        navigation_status_msg.orientation_rate.z = omega_t(2);
        navigation_status_msg.body_velocity.x = linearVelocity(0);
        navigation_status_msg.body_velocity.y = linearVelocity(1);
        navigation_status_msg.body_velocity.z = zdot_filtered(0);
        navigation_status_msg.altitude = static_cast<float>(altitude);
        navigation_filter_pub.publish(navigation_status_msg);

        if (configure) {
            ROS_INFO("Configuring frames for Navigation Filter Simulated Node");
            robot_model
                = robust_robot_model_factory.UpdateFrame(robot_model->GetPositionVector(robust::robotModelID::AUV),
                    robot_model->GetPositionVector(robust::robotModelID::RobustArmModel));
            configure = false;
        }
        if (simulate_camera_outputs_landing) {
            robust_msgs::NodulePositionLanding out_nodule;
            Eigen::TransfMatrix camera_T_nodule;
            Eigen::TransfMatrix camera_T_world
                = robot_model->GetTransformation(robust::robotModelID::cameraFrame_auv).inverse();
            Eigen::TransfMatrix pool_T_nodule;
            pool_T_nodule.SetTransl(nodule_position_pool_frame);
            camera_T_nodule = camera_T_world * world_T_pool * pool_T_nodule;
            out_nodule.position.x = camera_T_nodule.GetTransl()(0); // north
            out_nodule.position.y = camera_T_nodule.GetTransl()(1); // east
            out_nodule.position.z = camera_T_nodule.GetTransl()(2); // depth
            auv_land_nodule.publish(out_nodule);

            if (publish_nodule_position_land) {
                publish_nodule_position_land = false;
                std_msgs::Empty out_comand;
                auv_land_comand.publish(out_comand);
            }
        }
        if (simulate_camera_outputs_manipulation) {
            if (publish_nodule_manipulation) {
                if (iteration == 0) {
                    std::cout << "PUBLISHING NODULE POSITION MANIPULATION = " << position_base_frame.transpose()
                              << " <baseFrame>" << std::endl;
                    Eigen::Vector3d position_camera_frame
                        = camera_T_base.GetRotMatrix() * position_base_frame + camera_T_base.GetTransl();
                    Eigen::Vector3d normal_base_frame;
                    normal_base_frame(0) = 0.0;
                    normal_base_frame(1) = 0.0;
                    normal_base_frame(2) = -1.0;
                    Eigen::Vector3d normal_camera_frame
                        = camera_T_base.GetRotMatrix() * (normal_base_frame.normalized());
                    robust_msgs::NoduleWithNormal nodule_with_normal;
                    nodule_with_normal.normal.x = normal_camera_frame(0);
                    nodule_with_normal.normal.y = normal_camera_frame(1);
                    nodule_with_normal.normal.z = normal_camera_frame(2);
                    nodule_with_normal.position.x = position_camera_frame(0);
                    nodule_with_normal.position.y = position_camera_frame(1);
                    nodule_with_normal.position.z = position_camera_frame(2);
                    arm_nodule_position.publish(nodule_with_normal);
                    iteration++;
                } else if (iteration < 6) {
                    std::cout << "DO STEP " << iteration << std::endl;
                    iteration++;
                    std_msgs::Empty msg;
                    arm_do_step.publish(msg);

                } else if (iteration == 6) {
                    iteration = 0;
                    std_msgs::Empty msg;
                    arm_go_to_pre_inspection.publish(msg);
                    std::cout << "ENDED TOOL APPROACH GOING TO PRE INSPECTION" << std::endl;
                }

                publish_nodule_manipulation = false;
            }
        }

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

void DVLCallBack(const robust_msgs::DVLConstPtr& msg)
{
    DVLVelocity(0) = msg->linear_velocity_bottom_track.x;
    DVLVelocity(1) = msg->linear_velocity_bottom_track.y;
    DVLVelocity(2) = msg->linear_velocity_bottom_track.z;
    depth(0) = msg->depth - lower_folaga_distance;
    altitude = (floor_position - depth(0));
    position_world(2) = depth(0);
}

void CompassCallBack(const robust_msgs::OrientationDepthConstPtr& msg)
{

    rpy_old = rpy;
    rpy.SetRoll(msg->RPY.x / 180.0 * M_PI);
    rpy.SetPitch(msg->RPY.y / 180.0 * M_PI);
    rpy.SetYaw(msg->RPY.z / 180.0 * M_PI);

}

void StartDemoLanding(const std_msgs::EmptyConstPtr& msg)
{
    publish_nodule_position_land = true;
    ROS_INFO("STARTING DEMO LAND");
}

void ConfigCallback(robust_simulations::second_order_filterConfig& config, uint32_t level)
{

    Eigen::Vector2d filteredDataCoefficients_Depth;
    Eigen::Vector3d inputDataCoefficients_Depth;
    inputDataCoefficients_Depth(0) = config.a1_Depth;
    inputDataCoefficients_Depth(1) = config.a2_Depth;
    inputDataCoefficients_Depth(2) = config.a3_Depth;
    filteredDataCoefficients_Depth(0) = config.b1_Depth;
    filteredDataCoefficients_Depth(1) = config.b2_Depth;
    depthFilter.Init(1, filteredDataCoefficients_Depth, inputDataCoefficients_Depth, false);
    Eigen::Vector2d filteredDataCoefficients_Angular;
    Eigen::Vector3d inputDataCoefficients_Angular;
    inputDataCoefficients_Angular(0) = config.a1_Angular;
    inputDataCoefficients_Angular(1) = config.a2_Angular;
    inputDataCoefficients_Angular(2) = config.a3_Angular;
    filteredDataCoefficients_Angular(0) = config.b1_Angular;
    filteredDataCoefficients_Angular(1) = config.b2_Angular;
    angularDerivativesFilter.Init(3, filteredDataCoefficients_Angular, inputDataCoefficients_Angular, true);

    Eigen::Vector2d filteredDataCoefficients_XY;
    Eigen::Vector3d inputDataCoefficients_XY;
    inputDataCoefficients_XY(0) = config.a1_XY;
    inputDataCoefficients_XY(1) = config.a2_XY;
    inputDataCoefficients_XY(2) = config.a3_XY;
    filteredDataCoefficients_XY(0) = config.b1_XY;
    filteredDataCoefficients_XY(1) = config.b2_XY;
    xyFilter.Init(3, filteredDataCoefficients_XY, inputDataCoefficients_XY, false);
    velocity_input = config.VelocityInput;
    ROS_WARN("Loading dynamic reconfigure");
}

void ToolPositionCallBack(const robust_control::ToolPositionConstPtr& msg)
{
    Eigen::Vector3d tool_linear_position;
    rml::EulerRPY tool_RPY;
    tool_linear_position(0) = msg->XYZ.x;
    tool_linear_position(1) = msg->XYZ.y;
    tool_linear_position(2) = msg->XYZ.z;

    tool_RPY.SetRoll(msg->RPY.x);
    tool_RPY.SetPitch(msg->RPY.y);
    tool_RPY.SetYaw(msg->RPY.z);
    base_T_tool.SetTransl(tool_linear_position);
    base_T_tool.SetRotMatrix(tool_RPY.ToRotMatrix());
}

void CameraPositionCallBack(const geometry_msgs::PoseStampedConstPtr& msg)
{
    Eigen::TransfMatrix camera_T_auv
        = robot_model->GetTransformationFrames(robust::robotModelID::cameraFrame_auv, robust::robotModelID::AUV);
    Eigen::TransfMatrix pool_T_colorCamera;
    Eigen::Vector3d pool_position_camera_frame;
    pool_position_camera_frame(0) = msg->pose.position.x;
    pool_position_camera_frame(1) = msg->pose.position.y;
    pool_position_camera_frame(2) = msg->pose.position.z;
    pool_T_colorCamera.SetTransl(pool_position_camera_frame);
    Eigen::Quaterniond quaternion_pool_camera(
        msg->pose.orientation.w, msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z);
    Eigen::TransfMatrix colorCamera_T_camera;
    rml::EulerRPY rpy_color_camera(0.0, 0.0, -M_PI / 2);

*/
    colorCamera_T_camera.SetRotMatrix(rpy_color_camera.ToRotMatrix());
    Eigen::TransfMatrix world_T_pool;
    rml::EulerRPY rpy_world_pool(0.0, 0.0, 235 * (M_PI / 180));
    world_T_pool.SetRotMatrix(rpy_world_pool.ToRotMatrix());

    pool_T_colorCamera.SetRotMatrix(quaternion_pool_camera.toRotationMatrix());

    Eigen::TransfMatrix world_T_auv = world_T_pool * pool_T_colorCamera * colorCamera_T_camera * camera_T_auv;

    position_world(0) = world_T_auv.GetTransl()(0);
    position_world(1) = world_T_auv.GetTransl()(1);
}

void KclEventCallBack(const robust_msgs::EventConstPtr& msg)
{
    if (msg->event == msg->MOVE_TOOL_TO_NODULE_REACHED_POSITION) {
        publish_nodule_manipulation = true;
        std::cout << "Received movement finished move tool " << std::endl;
    }
    ROS_INFO("RECEIVED ACK MOVEMENT FINISHED");
}

void StartDemoTool(const std_msgs::EmptyConstPtr& msg)
{
    publish_nodule_manipulation = true;
    ROS_INFO("STARTING SIMULATION MOVE  TOOL TO NODULE");
}

void NodulePositionLandinCallBack(const robust_msgs::NodulePositionLandingConstPtr& msg)
{

    Eigen::Vector3d nodule_position_camera_frame;
    Eigen::Vector3d nodule_position_base_frame;
    Eigen::TransfMatrix base_T_camera = robot_model->GetTransformationFrames(
        robust::robotModelID::robot_base_id, robust::robotModelID::cameraFrame_auv);
    nodule_position_camera_frame(0) = msg->position.x;
    nodule_position_camera_frame(1) = msg->position.y;
    nodule_position_camera_frame(2) = msg->position.z;
    nodule_position_base_frame
        = base_T_camera.GetRotMatrix() * nodule_position_camera_frame + base_T_camera.GetTransl();

}

void NodulePositionManipulationCallBack(const robust_msgs::NoduleWithNormalConstPtr& msg)
{


    Eigen::Vector3d nodule_position_camera_frame;
    Eigen::Vector3d nodule_normal_camera_frame;
    Eigen::Vector3d nodule_position_base_frame;
    Eigen::Vector3d nodule_normal_base_frame;
    Eigen::TransfMatrix base_T_camera = robot_model->GetTransformationFrames(
        robust::robotModelID::robot_base_id, robust::robotModelID::cameraFrame_auv);

    nodule_position_camera_frame(0) = msg->position.x;
    nodule_position_camera_frame(1) = msg->position.y;
    nodule_position_camera_frame(2) = msg->position.z;

    nodule_normal_camera_frame(0) = msg->normal.x;
    nodule_normal_camera_frame(1) = msg->normal.y;
    nodule_normal_camera_frame(2) = msg->normal.z;

    nodule_position_base_frame
        = base_T_camera.GetRotMatrix() * nodule_position_camera_frame + base_T_camera.GetTransl();
    nodule_normal_base_frame = base_T_camera.GetRotMatrix() * nodule_normal_camera_frame;

}

void ConfigureKCL(const std_msgs::EmptyConstPtr& msg) { configure = true; }
