# Interview codes

##Contents
The following files are provided:

### TPIK
* include/tpik two headers of a cpp library.
 
###IKCL
* include/ikcl header of a cpp library.
*  src folder source file of a class of a cpp library.

### ROS_PACKAGE
* src folder a ros node.
* msg folder a msg file.
* cfg folder, two cfg files for dynamic reconfigure.
 
###ROS_PACKAGE2
* src/ControlNodes a ros node.
* launch folder a ros launch file.
* cfg folder, two cfg file for dynamic reconfigure.

CMakeLists.txt and package.xml file are provided but it is not possible either to compile or to run the code since only part of the code is loaded
in the repository.
