
#include "RobustFunctions/RosInterface.h"

#include "Defines/RobustRosDefines.h"
#include <geometry_msgs/Twist.h>
#include <rml/RML.h>
#include <robust_control/TestControl.h>
#include <robust_msgs/EffortPercentage.h>
#include <ros/ros.h>
#include <std_msgs/Empty.h>

void NavigationFilterCallBack(const auv_msgs::NavigationStatusConstPtr& msg);
void AttitudeControlCallBack(const robust_control::TestControlConstPtr& msg);
void ThrusterAngularCallBack(const robust_control::TestControlConstPtr& msg);
void LinearVelocityCallBack(const robust_control::TestControlConstPtr& msg);
void LinearThrusterCallBack(const robust_control::TestControlConstPtr& msg);
static Eigen::Vector3d desiredLinearVelocities;
static Eigen::Vector3d percentageThrusterLinear;
static Eigen::Vector3d percentageThrusterAngular;
static rml::EulerRPY desiredRPY;
static rml::EulerRPY rpy;
static double thAttitude;
static double Kp;
static int timeTh;
static bool timeToElapse;

enum class ControlType { AttitudePosition, AttitudeThruster, LinearThruster, LinearVelocity, IDLE };
static ControlType controlType;
static std::chrono::system_clock::time_point time_start;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "test_control_node");
    ros::NodeHandle nh;
    int rate = 10;
    double ts = 1.0 / static_cast<double>(rate);
    double z_thruster_perc = -12.0;
    controlType = ControlType::IDLE;
    bool enable_remote_axis = true;
    ros::Rate loop_rate(rate);
    // Ros publishers
    ros::Publisher auv_velocity_pub
        = nh.advertise<geometry_msgs::Twist>(robust::rosTopic::auv_velocity_request.c_str(), 10);
    ros::Publisher auv_effort_pub
        = nh.advertise<robust_msgs::EffortPercentage>(robust::rosTopic::llc::auv_axis_effort.c_str(), 10);
    ros::Publisher auv_remote_axis_pub
        = nh.advertise<std_msgs::Empty>(robust::rosTopic::llc::remote_axis_topic.c_str(), 10);
    ros::Subscriber auv_navigation_filter_sub
        = nh.subscribe(robust::rosTopic::auv_feedback_navigation_filter, 1, NavigationFilterCallBack);
    ros::Subscriber command_attitude_control_sub
        = nh.subscribe(robust::rosTopic::test_command_topic_attidue_control, 1, AttitudeControlCallBack);
    ros::Subscriber command_thruster_angular_sub
        = nh.subscribe(robust::rosTopic::test_command_topic_thruster_orientation_control, 1, ThrusterAngularCallBack);
    ros::Subscriber command_linear_velocity_sub
        = nh.subscribe(robust::rosTopic::test_command_topic_linear_velocity, 1, LinearVelocityCallBack);
    ros::Subscriber command_thruster_linear_sub
        = nh.subscribe(robust::rosTopic::test_command_topic_thrusther_linear, 1, LinearThrusterCallBack);

    thAttitude = 0.2;
    timeTh = 30000;
    timeToElapse = false;
    Kp = 0.2;
    std::string log_string = "TEST CONTROL NODE LAUNCHED, PUBLISH COMMAND IN TOPIC :\n";
    std::cout << log_string << robust::rosTopic::test_command_topic_attidue_control << "\n"
              << robust::rosTopic::test_command_topic_thruster_orientation_control << "\n"
              << robust::rosTopic::test_command_topic_linear_velocity << "\n"
              << robust::rosTopic::test_command_topic_thrusther_linear << "\n"
              << std::endl;
    int count = 0;
    std::vector<std::string> live = { "\\", "|", "/", "--", "\\", "|", "/", "--" };
    int loopsOrient = 0;

    while (nh.ok()) {

        if (enable_remote_axis) {
            std::cout << "publishing enable remote axis " << std::endl;
            std_msgs::Empty remoteAxisEnabled;
            auv_remote_axis_pub.publish(remoteAxisEnabled);
            enable_remote_axis = false;
        }
        Eigen::Vector6d y;

        switch (controlType) {
        case ControlType::AttitudePosition: {
            Eigen::Vector3d error = rml::VersorLemma(rpy.ToRotMatrix(), desiredRPY.ToRotMatrix());
            y.setZero();
            y(5) = z_thruster_perc;
            y.segment(0, 3) = Kp * error;
            count++;
            if (count % 1000 == 0) {
                std::cout << "\033[1;34m"
                          << "Aligning" << std::setw(7) << live.at(loopsOrient % 8) << "\033[0m"
                          << "\r" << std::flush;
                loopsOrient++;
                count = 0;
            }
            if (std::fabs(error.norm()) < thAttitude) {
                ROS_INFO("Heading Aligned, switching to IDLE");
                controlType = ControlType::IDLE;
                count = 0;
            }

            geometry_msgs::Twist twist_publish;
            twist_publish = FromVectorAngLinToTwist(y);
            auv_velocity_pub.publish(twist_publish);
        } break;
        case ControlType::AttitudeThruster: {
            robust_msgs::EffortPercentage effort_percentage_msg;
            effort_percentage_msg.angular.x = percentageThrusterAngular(0);
            effort_percentage_msg.angular.y = percentageThrusterAngular(1);
            effort_percentage_msg.angular.z = percentageThrusterAngular(2);
            effort_percentage_msg.linear.x = 0.0;
            effort_percentage_msg.linear.y = 0.0;
            effort_percentage_msg.linear.z = z_thruster_perc; // 0.0;
            std::chrono::duration<double, std::milli> diff
                = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now())
                - time_start;
            if (count % 1000 == 0) {
                std::cout.precision(3);
                std::cout.width(7);
                std::cout << " \033[1;34mtime elapsed = \033[0m " << std::setw(4) << (int)diff.count() / 1000
                          << "   (seconds)\r" << std::flush;
                count = 0;
            }
            if (diff.count() > timeTh) {
                timeToElapse = false;
                count = 0;
                ROS_INFO("Time Elapsed, switching to IDLE");
                controlType = ControlType::IDLE;
            }
            auv_effort_pub.publish(effort_percentage_msg);

        } break;
        case ControlType::LinearThruster: {
            robust_msgs::EffortPercentage effort_percentage_msg;
            effort_percentage_msg.angular.x = 0.0;
            effort_percentage_msg.angular.y = 0.0;
            effort_percentage_msg.angular.z = 0.0;
            effort_percentage_msg.linear.x = percentageThrusterLinear(0);
            effort_percentage_msg.linear.y = percentageThrusterLinear(1);
            effort_percentage_msg.linear.z = percentageThrusterLinear(2);
            std::chrono::duration<double, std::milli> diff
                = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now())
                - time_start;
            if (count % 1000 == 0) {
                std::cout.precision(3);
                std::cout.width(7);
                std::cout << " \033[1;34mtime elapsed = \033[0m " << std::setw(4) << (int)diff.count() / 1000
                          << "   (seconds)\r" << std::flush;
                count = 0;
            }
            if (diff.count() > timeTh) {
                timeToElapse = false;
                count = 0;
                ROS_INFO("Time Elapsed, switching to IDLE");
                controlType = ControlType::IDLE;
            }
            auv_effort_pub.publish(effort_percentage_msg);
        } break;
        case ControlType::LinearVelocity: {
            count++;
            y.setZero();
            y.segment(3, 3) = desiredLinearVelocities;
            std::chrono::duration<double, std::milli> diff
                = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now())
                - time_start;
            if (count % 1000 == 0) {
                std::cout.precision(3);
                std::cout.width(7);
                std::cout << " \033[1;34mtime elapsed = \033[0m " << std::setw(4) << (int)diff.count() / 1000
                          << "   (seconds)\r" << std::flush;
                count = 0;
            }
            if (diff.count() > timeTh) {
                timeToElapse = false;
                count = 0;
                ROS_INFO("Time Elapsed, switching to IDLE");
                controlType = ControlType::IDLE;
            }

            geometry_msgs::Twist twist_publish;
            twist_publish = FromVectorAngLinToTwist(y);
            auv_velocity_pub.publish(twist_publish);

        } break;
        case ControlType::IDLE: {
            y.setZero();
            robust_msgs::EffortPercentage effort_percentage_msg;
            effort_percentage_msg.angular.x = 0.0;
            effort_percentage_msg.angular.y = 0.0;
            effort_percentage_msg.angular.z = 0.0;
            effort_percentage_msg.linear.x = 0.0;
            effort_percentage_msg.linear.y = 0.0;
            effort_percentage_msg.linear.z = z_thruster_perc; // 0.0;
            auv_effort_pub.publish(effort_percentage_msg);
        } break;
        }
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}

void NavigationFilterCallBack(const auv_msgs::NavigationStatusConstPtr& msg)
{

    rpy.SetRoll(msg->orientation.x);
    rpy.SetPitch(msg->orientation.y);
    rpy.SetYaw(msg->orientation.z);
    desiredRPY.SetRoll(rpy.GetRoll());
    desiredRPY.SetPitch(rpy.GetPitch());
}

void AttitudeControlCallBack(const robust_control::TestControlConstPtr& msg)
{
    ROS_INFO("ATTITUDE CONTROL MSG RECEIVED");
    controlType = ControlType::AttitudePosition;
    desiredRPY.SetRoll(msg->r);
    desiredRPY.SetPitch(msg->p);
    desiredRPY.SetYaw(msg->y);
    Kp = msg->Kp;
    thAttitude = msg->angularThreshold;
}

void ThrusterAngularCallBack(const robust_control::TestControlConstPtr& msg)
{
    ROS_INFO("ANGULAR THRUSTER MSG RECEIVED");
    controlType = ControlType::AttitudeThruster;
    percentageThrusterAngular(0) = msg->angularThrusterPercentage.x;
    percentageThrusterAngular(1) = msg->angularThrusterPercentage.y;
    percentageThrusterAngular(2) = msg->angularThrusterPercentage.z;
    timeTh = msg->timeThreshold;
    time_start = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
}
void LinearVelocityCallBack(const robust_control::TestControlConstPtr& msg)
{
    ROS_INFO("LINEAR VELOCITY MSG RECEIVED");
    controlType = ControlType::LinearVelocity;
    desiredLinearVelocities(0) = msg->linearVelocities.x;
    desiredLinearVelocities(1) = msg->linearVelocities.y;
    desiredLinearVelocities(2) = msg->linearVelocities.z;
    timeTh = msg->timeThreshold;
    time_start = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
}
void LinearThrusterCallBack(const robust_control::TestControlConstPtr& msg)
{
    ROS_INFO("LINEAR THUSTER MSG RECEIVED");
    controlType = ControlType::LinearThruster;
    percentageThrusterLinear(0) = msg->linearThrusterPercentage.x;
    percentageThrusterLinear(1) = msg->linearThrusterPercentage.y;
    percentageThrusterLinear(2) = msg->linearThrusterPercentage.z;
    timeTh = msg->timeThreshold;
    time_start = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
}
